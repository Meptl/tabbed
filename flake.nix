{
  description = "Simple generic tabbed fronted to xembed aware applications";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-21.11";
  };

  outputs = { self, nixpkgs, flake-utils }:
  let
    system = "x86_64-linux";
    pkgs = import nixpkgs { inherit system; };
  in rec {
    packages.x86_64-linux.tabbed = pkgs.stdenv.mkDerivation rec {
      name = "tabbed";
      src = ./.;

      nativeBuildInputs = [ ];
      buildInputs = with pkgs.xorg; [ xorgproto libX11 libXft ];

      makeFlags = [ "CC:=$(CC)" ];

      installFlags = [ "PREFIX=$(out)" ];
    };
    defaultPackage.x86_64-linux = packages.x86_64-linux.tabbed;
  };
}
